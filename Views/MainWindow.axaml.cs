using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Markup.Xaml;
using NAudio.Wave;

namespace LabW4.Views;

public partial class MainWindow : Window
{
    public Image? CarImage;
    public double NewPosition;
    public WaveOutEvent? WaveOut;
    public AudioFileReader? AudioFileReader;


    
    public MainWindow()
    {
        InitializeComponent();
        CarImage = this.FindControl<Image>("Car");
        WaveOut = new WaveOutEvent();
        
        KeyDown += PressButton!;
    }
    
    private void InitializeComponent()
    {
        AvaloniaXamlLoader.Load(this);
    }
    
    private void PressButton(object sender, KeyEventArgs e)
    {
        MoveCar(sender, e);
    }

    public void MoveCar(object sender, KeyEventArgs e)
    {
        LoadAudio();
        switch (e.Key)
        {
            case Key.Up:
                MoveUp(10);
                break;
            case Key.Down:
                MoveDown(10);
                break;
        }
        WaveOut?.Play();
    }

    public void MoveUp(double up)
    {
        LoadAudio();
        if (CarImage != null)
        {
            var currentPosition = Canvas.GetTop(CarImage);
            NewPosition = currentPosition - up;
            
            if (NewPosition < 0)
            {
                NewPosition = 0;
            }

            Canvas.SetTop(CarImage, NewPosition);
        }
    }

    public void MoveDown(double down)
    {
        if (CarImage != null)
        {
            NewPosition = Canvas.GetTop(CarImage) + down;
            
            if(!double.IsNaN(NewPosition))
                Canvas.SetTop(CarImage, NewPosition);
            else
                Canvas.SetTop(CarImage, down);
        }
    }
    
    public void LoadAudio()
    {
        string audioFilePath = "../../../Assets/car-acceleration-inside-car-7087.mp3";

        if (WaveOut?.PlaybackState == PlaybackState.Playing)
        {
            WaveOut.Stop();
        }

        AudioFileReader = new AudioFileReader(audioFilePath);
        WaveOut?.Init(AudioFileReader);
    }

}